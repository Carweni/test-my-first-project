/*This code puts three number in ascending order.
  The programming language used for this program is C.
  The sentences were built in Portuguese(BR).
  The operational system is Windows*/

int main(void)
{
    int num1, num2, num3, menor, meio, maior;

    printf("Informe o primeiro numero inteiro: ");
    scanf("%d",&num1);
    printf("Informe o segundo numero inteiro: ");
    scanf("%d",&num2);
    printf("Informe o terceiro numero inteiro: ");
    scanf("%d",&num3);

    if (num1 > num2 && num1 > num3)
    {
        maior = num1;
    }
    else if (num2 > num1 && num2 > num3)
    {
        maior = num2;
    }
    else
    {
        maior = num3;
    }

     if ((num1 > num2 && num1 < num3) || (num1 < num2 && num1 > num3))
    {
        meio = num1;
    }
    else if ((num2 > num1 && num2 < num3)|| (num2 < num1 && num2 > num3))
    {
        meio = num2;
    }
    else
    {
        meio = num3;
    }

    if (num1 < num2 && num1 < num3)
    {
        menor = num1;
    }
    else if (num2 < num1 && num2 < num3)
    {
        menor = num2;
    }
    else
    {
        menor = num3;
    }

    printf("\nMenor: %d\nMeio: %d\nMaior: %d\n", menor, meio, maior);

    return 0;
}
